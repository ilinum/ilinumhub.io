	var map = AmCharts.makeChart("mapdiv",{
		type: "map",
		theme: "dark",
		pathToImages     : "http://cdn.amcharts.com/lib/3/images/",
		panEventsEnabled : true,
		backgroundColor  : "#FFFFFF",
		backgroundAlpha  : 1,

		zoomControl: {
			panControlEnabled  : true,
			zoomControlEnabled : true
		},

		dataProvider     : {
			map             : "worldHigh",
			getAreasFromMap : true,
			areas           :
[
	{
		"id": "AT",
		"showAsSelected": true
	},
	{
		"id": "BE",
		"showAsSelected": true
	},
	{
		"id": "CZ",
		"showAsSelected": true
	},
	{
		"id": "DE",
		"showAsSelected": true
	},
	{
		"id": "EE",
		"showAsSelected": true
	},
	{
		"id": "FI",
		"showAsSelected": true
	},
	{
		"id": "FR",
		"showAsSelected": true
	},
	{
		"id": "GB",
		"showAsSelected": true
	},
	{
		"id": "GR",
		"showAsSelected": true
	},
	{
		"id": "IT",
		"showAsSelected": true
	},
	{
		"id": "LT",
		"showAsSelected": true
	},
	{
		"id": "LV",
		"showAsSelected": true
	},
	{
		"id": "ME",
		"showAsSelected": true
	},
	{
		"id": "NL",
		"showAsSelected": true
	},
	{
		"id": "SE",
		"showAsSelected": true
	},
	{
		"id": "TR",
		"showAsSelected": true
	},
	{
		"id": "RU",
		"showAsSelected": true
	},
	{
		"id": "BZ",
		"showAsSelected": true
	},
	{
		"id": "GT",
		"showAsSelected": true
	},
	{
		"id": "MX",
		"showAsSelected": true
	},
	{
		"id": "US",
		"showAsSelected": true
	},
	{
		"id": "EG",
		"showAsSelected": true
	}
]
		},

		areasSettings    : {
			autoZoom             : true,	
			color                : "#F0F0F0",
			colorSolid           : "#CF5300",
			selectedColor        : "#CF5300",
			outlineColor         : "#666666",
			rollOverColor        : "#B4B4B7",
			rollOverOutlineColor : "#000000"
		}
	});
